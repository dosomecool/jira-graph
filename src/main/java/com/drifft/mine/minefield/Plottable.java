package com.drifft.mine.minefield;

public interface Plottable {

	public Data getPmfData();
	public Data getCdfData();
	public Data getSurvivalData(); 
	public Data getHazardData();
}
