package com.drifft.mine.gui;

import java.io.IOException;

import com.jfoenix.controls.JFXButton;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;

public class Menu extends AnchorPane {

    @FXML
    private JFXButton loadTemplateButton;

    @FXML
    private JFXButton loadDataButton;

	private Controller controller;

	public Menu(Controller controller) {
		this.controller = controller;

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("Menu.fxml"));
			loader.setController(this);
			loader.setRoot(this);
			loader.load();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	public void initialize() {
		
		loadTemplateButton.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override 
			public void handle(ActionEvent event) {
				
				controller.loadTemplate();
				event.consume();
			}
		});
		
		loadDataButton.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override 
			public void handle(ActionEvent event) {
				
				controller.loadData();
				event.consume();
			}
		});
	}
}
