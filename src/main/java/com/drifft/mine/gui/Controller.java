package com.drifft.mine.gui;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.drifft.mine.minefield.Data;

import netscape.javascript.JSException;
import netscape.javascript.JSObject;

public class Controller {

	private Root rootPane; 
	private Menu menu; 

	private final URL templateUrl;
	private JSObject jsLink;

	private Data data;

	public Controller() {
		this.rootPane = new Root(this); 
		this.menu = new Menu(this);
		this.rootPane.setTop(this.menu);
		this.templateUrl = this.getClass().getResource("/com/drifft/mine/gui/html/template.html");
	}

	public void writeHtmlToFile(String filename, String html) {
		
		try {
			FileWriter fstream = new FileWriter(filename);
			BufferedWriter out = new BufferedWriter(fstream);
			out.write(html);
			out.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void loadTemplate() {
		System.out.print("Loading template ... \n");
		File input = new File(this.templateUrl.getFile());
		Document doc = null;
		try {
			doc = Jsoup.parse(input, "UTF-8");
		} catch (IOException e) {
			e.printStackTrace();
		}
		doc.outputSettings().indentAmount(2); 
		String temp = doc.toString();		
		rootPane.getTextArea().setText(temp);
	}

	public void loadContentInWebEngine() {
		System.out.print("Loading html code ... \n");
		String html = rootPane.getTextArea().getText();
		rootPane.getWebEngine().loadContent(html); 
	}

	public void link() {
		try {
			jsLink = (JSObject) rootPane.getWebEngine().executeScript("getLink()");
		} catch(JSException je) {
			System.out.print("Could not link with javascript! \n");
		}
	}

	public void loadData() {
		jsLink.call("graph", Arrays.toString(data.getData().toArray()));
	}
	
	public Root getRootPane() {
		return rootPane;
	}

	public Menu getMenu() {
		return menu;
	}

	public void query() {
		String url   = rootPane.getUrlTextfield().getText();
		String email = rootPane.getEmailTextfield().getText();
		String token = rootPane.getTokenTexfield().getText();
		String jql   = rootPane.getJqlTexfield().getText();		
		JiraQuery jira = new JiraQuery();
		jira.addQuery(jql);
		jira.query(email, token, url);
	}
}
