package com.drifft.mine.gui;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.ObjectMapper;
import kong.unirest.Unirest;

public class JiraQuery {

	// The payload definition using the Jackson library
	private JsonNodeFactory jnf = JsonNodeFactory.instance;
	private ObjectNode payload = jnf.objectNode();
	private ArrayNode queries = payload.putArray("queries");

	public JiraQuery() {
	}

	public void addQuery(String jql) {
		queries.add(jql);
	}

	public void query(String email, String apitoken, String jiraUrl) {
		// Connect Jackson ObjectMapper to Unirest
		Unirest.config().setObjectMapper(new ObjectMapper() {
			private com.fasterxml.jackson.databind.ObjectMapper jacksonObjectMapper
			= new com.fasterxml.jackson.databind.ObjectMapper();

			public <T> T readValue(String value, Class<T> valueType) {
				try {
					return jacksonObjectMapper.readValue(value, valueType);
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
			}

			public String writeValue(Object value) {
				try {
					return jacksonObjectMapper.writeValueAsString(value);
				} catch (JsonProcessingException e) {
					throw new RuntimeException(e);
				}
			}
		});

		HttpResponse<JsonNode> response = Unirest.post(jiraUrl + "/rest/api/3/jql/parse")
				.basicAuth(email, apitoken)
				.header("Accept", "application/json")
				.header("Content-Type", "application/json")
				.body(payload)
				.asJson();

		System.out.println(response.getBody());
	}
}
