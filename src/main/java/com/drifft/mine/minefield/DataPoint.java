package com.drifft.mine.minefield;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;

public class DataPoint {

	private int id; 
	private int x; 
	private int y;
	private double z; 
	private double style; 
	
	public DataPoint(int id, int x, int y, double z, double style) {
		this.id = id;
		this.x = x;
		this.y = y;
		this.z = z; 
		this.style = style;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public double getZ() {
		return z;
	}

	public void setZ(double z) {
		this.z = z;
	}

	public double getStyle() {
		return style;
	}

	public void setStyle(double style) {
		this.style = style;
	}
	
	@Override 
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("id: ").append(id);
		sb.append("\nx: ").append(x);
		sb.append("\ny: ").append(y);
		sb.append("\nz: ").append(z);
		sb.append("\nstyle: ").append(style);
		
		String jsonStr = null; 
		ObjectMapper mapper = new ObjectMapper();
		
        try {
            jsonStr = mapper.writeValueAsString(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
		return jsonStr;
	}
}
