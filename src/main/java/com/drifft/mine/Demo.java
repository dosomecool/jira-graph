package com.drifft.mine;

import java.io.IOException;

import com.drifft.mine.gui.Controller;
import com.jfoenix.controls.JFXDecorator;
import com.sun.javafx.application.LauncherImpl;

import javafx.application.Application;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class Demo extends Application {

	@Override 
	public void init() throws IOException {               
	}
	
	@Override 
	public void start(Stage stage) {		
         
		Controller controller = new Controller();
	
		JFXDecorator decorator = new JFXDecorator(stage, controller.getRootPane());
		Rectangle2D visualBounds = Screen.getPrimary().getVisualBounds();
		double width = visualBounds.getWidth();
		double height = visualBounds.getHeight();
		
		Scene scene = new Scene(decorator, width, height);
					
		stage.getIcons().add(new Image(getClass().getResource("/com/drifft/mine/gui/media/favicon.png").toExternalForm()));		
		stage.setTitle("DRIFFT - MINE OPTIMIZATION");
		stage.setScene(scene);
		stage.show();
	}
	
	public static void main(String[] args) {
		LauncherImpl.launchApplication(Demo.class, args);
	}
}
