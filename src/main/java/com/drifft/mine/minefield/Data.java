package com.drifft.mine.minefield;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;

public class Data {

	private List<DataPoint> data = new ArrayList<>();
	
	public Data() {
	}

	public Data(List<DataPoint> data) {
		this.data = data;
	}
	
	public void addPoint(DataPoint point) {
		this.data.add(point); 
	}
	
	public List<DataPoint> getData() {
		return data;
	}

	public void setData(List<DataPoint> data) {
		this.data = data;
	}

	@Override 
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("data: ").append(data);

		String jsonStr = null; 
		ObjectMapper mapper = new ObjectMapper();
		
        try {
            jsonStr = mapper.writeValueAsString(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
		return jsonStr;
	}
}
