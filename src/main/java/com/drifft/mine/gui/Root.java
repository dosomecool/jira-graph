package com.drifft.mine.gui;

import java.io.IOException;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;

import javafx.concurrent.Worker.State;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.CacheHint;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

public class Root extends BorderPane {

    @FXML
    private BorderPane root;

    @FXML
    private SplitPane splitPane;

    @FXML
    private AnchorPane pageAnchorPane;

    @FXML
    private JFXTextField urlTextfield;

    @FXML
    private JFXTextField emailTextfield;

    @FXML
    private JFXPasswordField tokenTexfield;

    @FXML
    private JFXTextField jqlTexfield;

    @FXML
    private JFXButton queryButton;

    @FXML
    private WebView webView;

    @FXML
    private AnchorPane editorAnchorPane;

    @FXML
    private JFXTextArea jsonTextArea;

    @FXML
    private JFXTextArea textArea;
    
	private Controller controller; 
	private WebEngine webEngine; 
	
	public Root(Controller controller) {
		this.controller = controller;
		
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("Root.fxml"));
			loader.setController(this);
			loader.setRoot(this);
			loader.load();

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		this.getStylesheets().addAll(getClass().getResource("DrifftStylesheet.css").toExternalForm());
	}

	@FXML
	public void initialize() {
		
		textArea.setStyle(
			    "-fx-font: 18 Consolas;"
			    + "-fx-border-color: brown; "
			    + "-fx-border-style: dotted;"
			    + "-fx-border-width: 2;"
			);
		
		textArea.textProperty().addListener((observable, oldValue, newValue) -> {
			controller.loadContentInWebEngine();
		});
		
		webView.setCache(true);
		webView.setCacheHint(CacheHint.SPEED);
        webEngine = webView.getEngine();
        webEngine.setJavaScriptEnabled(true);
        // this is fired when engine.load() is called
        webEngine.getLoadWorker().stateProperty().addListener((observable, oldValue, newValue) -> {
            
            if (newValue == State.SUCCEEDED) {
            	controller.link();
            }
        });
        
        queryButton.setOnAction(new EventHandler<ActionEvent>() {
            
        	@Override 
            public void handle(ActionEvent event) {
        		controller.query();
        		event.consume();
            }
        });
	}

	public BorderPane getRoot() {
		return root;
	}

	public SplitPane getSplitPane() {
		return splitPane;
	}

	public AnchorPane getPageAnchorPane() {
		return pageAnchorPane;
	}

	public JFXTextField getUrlTextfield() {
		return urlTextfield;
	}

	public JFXTextField getEmailTextfield() {
		return emailTextfield;
	}

	public JFXPasswordField getTokenTexfield() {
		return tokenTexfield;
	}

	public JFXTextField getJqlTexfield() {
		return jqlTexfield;
	}

	public JFXButton getQueryButton() {
		return queryButton;
	}

	public WebView getWebView() {
		return webView;
	}

	public AnchorPane getEditorAnchorPane() {
		return editorAnchorPane;
	}

	public JFXTextArea getJsonTextArea() {
		return jsonTextArea;
	}

	public JFXTextArea getTextArea() {
		return textArea;
	}

	public Controller getController() {
		return controller;
	}

	public WebEngine getWebEngine() {
		return webEngine;
	}
}
